//
//  CreateFrameOrCollageViewController.m
//  TajMahalApp
//
//  Created by TIKAM CHANDRAKAR on 06/07/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "CreateFrameOrCollageViewController.h"
#import "NavigationController.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "ShapeCollectionViewCell.h"
#import "StickerCollectionViewCell.h"

#define kShapeSize1 CGSizeMake(160.f, 160.f)
#define kShapeSize2 CGSizeMake(150.f, 150.f)
#define kLeftPaddingForShape 20
#define kTopPaddingForShape 30
#define kResizeImage CGRectMake(40.f,40.f,120.f, 120.f)

#define kPhotoContainer CGRectMake(20.f,20.f,200.f, 200.f)


enum ENUM_ACTIVE_VIEW
{
    ENUM_SHAPE_VIEW = 1,
    ENUM_STICKER_VIEW = 2
};


enum ENUM_ACTIVE_SHAPE
{
    ENUM_SHAPE_1 = 1,
    ENUM_SHAPE_2 = 2
};


@interface CreateFrameOrCollageViewController ()
{
    IBOutlet UIView *subView, *shapeCollectionView, *fontView, *shapeView1, *shapeView2;  
    
    IBOutlet UICollectionView *shapeOrStickerCollectionView;
    IBOutlet UIImageView *backgroundImageView;
    
    UITextField *inputText, *fontType, *fontSize;
    NSString *selectedColor;
    NSNumber *value;
    
    NSArray *resourceList;
    NSInteger activeView; // 1 for sticker 2 for shape;
    ResouceController *resource;
    
    /* shapeView 1*/
    IBOutlet UIButton *shapeBackgroundImage;// selected from shape options
    IBOutlet UIImageView *imageContainer; // Selected from gallery
    
    
    CameraGalleryController *controller;
    
    UIImage *shapImage;
    UIImage *pickerImage;
    
    
    // Gesture Related Variables
    float firstX;
    float firstY;    
}

@end

@implementation CreateFrameOrCollageViewController

- (void)viewDidLoad
{
    [Utility resizeViewForiPhoneAndiPod:self.view];
    resource  = [[ResouceController alloc] init];
     controller = [[CameraGalleryController alloc] init];
    pickerImage = nil;
    [super viewDidLoad];
    //float   angle = M_PI/2;  //rotate 180°, or 1 π radians
   // self.view.layer.transform = CATransform3DMakeRotation(angle, 0, 0.0, 1.0);
    [self setUserInterface];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    [self setBackgroundImage];
    
    
//    
   //[[NavigationController sharedNavigationController] supportedInterfaceOrientations];
}
#pragma mark- User Define Method
- (void)setBackgroundImage
{
    DebugLog(@"Selected Image %@",_selectedImageName);
    [backgroundImageView setImage:[UIImage imageNamed:_selectedImageName]];
}
- (void)setUserInterface
{
   // [self setPotionForView:shapeView1];
    
     [self setPotionForView:imageContainer];
    if(GetAppDelegate().frameStatus == ENUM_COLLAGE_SELECTED)
    {
      [self setPotionForView:shapeView2];
    }
    
    [shapeOrStickerCollectionView registerNib:[UINib nibWithNibName:@"ShapeCollectionViewCell" bundle:nil]
                    forCellWithReuseIdentifier:@"ShapeCell"];
    [shapeOrStickerCollectionView registerNib:[UINib nibWithNibName:@"StickerCollectionViewCell" bundle:nil]
                   forCellWithReuseIdentifier:@"StickerCell"];
    
    
}

- (CGRect) getPosition:(UIView *) view
{
    if(view ==shapeView1)
    {
        NSLog(@"Shape Postion");
        return CGRectMake(kLeftPaddingForShape, kTopPaddingForShape, kShapeSize1.width,kShapeSize1.height);
    }
    else if(view == imageContainer)
    {
        return CGRectMake(kLeftPaddingForShape, kTopPaddingForShape, kShapeSize1.width,kShapeSize1.height);
    }
    
    else if(view == shapeView2)
    {
     return CGRectMake([Utility deviceHeight] - kShapeSize2.width- kLeftPaddingForShape, kTopPaddingForShape, kShapeSize2.width,kShapeSize1.height);
    }
    
    else if(view == shapeCollectionView)
    {
       return CGRectMake([Utility getYpoint:shapeCollectionView.frame.size.width frameHeight:[Utility deviceHeight]], 0, shapeCollectionView.frame.size.width, shapeCollectionView.frame.size.height);
    }
    
    return CGRectMake(0, 0, 0,0);
}

- (void)setPotionForView: (UIView *)view
{
    [view setFrame:[self getPosition:view]];
    [self.view addSubview:view];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)setShape :(NSInteger )activeViewType  orStickerView:(NSString *) viewType
{
    resourceList = nil;
    resourceList = [resource getListOfResource:viewType];
    activeView = activeViewType;
    [shapeCollectionView setFrame:[self getPosition:shapeCollectionView]];
    [subView addSubview:shapeCollectionView];
    [shapeOrStickerCollectionView reloadData];
    
}

- (void) setClipToBound:(UIImage *)maskImage {
    
    CALayer *mask = [CALayer layer];
    mask.contents = (id)[maskImage CGImage];
    mask.frame = CGRectMake(0, 0, imageContainer.frame.size.width,  imageContainer.frame.size.height);
    imageContainer.layer.mask = mask;
    imageContainer.layer.masksToBounds = YES;
    [imageContainer setClipsToBounds:YES];
    [self.view addSubview:imageContainer];
}

#pragma mark- IBActions
- (IBAction)backButtonClick
{
    [[NavigationController sharedNavigationController] popViewControllerAnimated:YES];
}

- (IBAction)changeShape
{
    [self setShape:ENUM_SHAPE_VIEW orStickerView:kShapeName];
}

- (IBAction)addPicture:(id) semder
{
    DebugLog(@"addPicture ");

    if(pickerImage == nil)
    {
        [self selectPicture];
    }
}

- (IBAction)removePicture:(id) semder
{
    DebugLog(@"Remove Picture ");
    [Utility showAlertViewWithTitle:@""
                            message:@"Delete Picture"
                           delegate:self
                       buttonTitles:[NSArray arrayWithObjects:kCancel,@"Delete", nil]
                  cancelButtonIndex:0 tagValue:ENUM_TAG_REMOVE_PICTURE];
}

- (IBAction)addText
{
  
}
- (IBAction)addSticker
{
    [self setShape:ENUM_STICKER_VIEW orStickerView:kStickerName];
}

- (IBAction)DrawOnPhoto
{
    
}

- (IBAction)saveImage
{
   
}

- (IBAction)textDone
{
    
}

- (IBAction)drawPhotoDone
{
    
}


#pragma mark- Orientation Handlling.

-(BOOL)shouldAutorotate
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    DebugLog(@"Current Orientation %ld",(long)orientation);
    
    if(orientation == UIDeviceOrientationPortrait)
    {
        return NO;
    }
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    NSUInteger supportedOrientations = UIInterfaceOrientationMaskLandscape;
    return supportedOrientations;
}



#pragma mark- Collection View  delegate and call

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    DebugLog(@"Count %lu",(unsigned long)resourceList.count);
    return resourceList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(activeView == ENUM_SHAPE_VIEW)
    {
        ShapeCollectionViewCell *shapeCell  =  [collectionView dequeueReusableCellWithReuseIdentifier:@"ShapeCell" forIndexPath:indexPath];
;
         [self prepareShapeCell:shapeCell forImage: [resourceList  objectAtIndex:indexPath.row]  indexPath: indexPath];
        return shapeCell;
    }
    else
    {
        StickerCollectionViewCell *stickerCell  =  [collectionView dequeueReusableCellWithReuseIdentifier:@"StickerCell" forIndexPath:indexPath];
       [self prepareStickerCell:stickerCell forImage: [resourceList  objectAtIndex:indexPath.row]  indexPath: indexPath];
        
        return stickerCell;
    }
    
}
- (void) prepareStickerCell:(StickerCollectionViewCell*)cell forImage:(NSString*)bgImage indexPath: (NSIndexPath *) indexPath
{
    cell.backgroundImage.image = nil;
    cell.backgroundImage.image = [UIImage imageNamed:bgImage];
    
}

- (void) prepareShapeCell:(ShapeCollectionViewCell*)cell forImage:(NSString*)bgImage indexPath: (NSIndexPath *) indexPath
{
    DebugLog(@"Count %@",bgImage);
    cell.backgroundImage.image = nil;
    cell.backgroundImage.image = [UIImage imageNamed:bgImage];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(activeView == ENUM_SHAPE_VIEW)
    {
        [imageContainer removeFromSuperview];
      // Reset the postion for shap view
        
        [self setPotionForView:imageContainer];
       
        shapImage = [UIImage imageNamed:[resourceList  objectAtIndex:indexPath.row]];
        
        
         [self setClipToBound:shapImage];
        
        imageContainer.image = shapImage;
       
        
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(addPicture:)];
        singleFingerTap.numberOfTapsRequired = 1;
        [imageContainer addGestureRecognizer:singleFingerTap];
        
      
        // Delete the added picture
        UITapGestureRecognizer *doubleTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(removePicture:)];
        doubleTap.numberOfTapsRequired = 2;
        [imageContainer addGestureRecognizer:doubleTap];
        [self.view addSubview:imageContainer];
        

    }
    else
    {
        
    }
}

#pragma mark- Camera And  Gallery delegate
- (void) mediaError:(NSString *) errorMessage errorCode:(NSInteger) code
{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:errorMessage
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    [myAlertView show];
    return;
}
- (void)cameraPermissionGranted
{
    [GetAppDelegate() hideLoadingView];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)galleryPermissionGranted
{
    [GetAppDelegate() hideLoadingView];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)takePhoto:(UIButton *)sender
{
    [GetAppDelegate() showLoadingViewWithMsg:kPleaseWait];
    [controller takePhotoFromCamera:self];
}
- (IBAction)selectPhoto:(UIButton *)sender
{
    [GetAppDelegate() showLoadingViewWithMsg:kPleaseWait];
    [controller takePhotoFromGallery:self];
}
- (void)selectPicture
{
    //[self.view addSubview:_fadeView];
    [Utility showAlertViewWithTitle:@""
                            message:@"Select Picture"
                           delegate:self
                       buttonTitles:[NSArray arrayWithObjects:kCancel,@"Camera",@"Gallery", nil]
                  cancelButtonIndex:0 tagValue:ENUM_TAG_PICTURE_SET];
}


#pragma mark- Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [GetAppDelegate() hideLoadingView];
     pickerImage = info[UIImagePickerControllerOriginalImage];
     pickerImage = [pickerImage rotate:pickerImage andOrientation:pickerImage.imageOrientation];
    
    //imageContainer.image = pickerImage;
    
    if(UIViewContentModeScaleAspectFit == [pickerImage getContentMode:kResizeImage.size])
    {
        pickerImage = [pickerImage imageByScalingToSize:kResizeImage.size contentMode:UIViewContentModeScaleAspectFit];
        DebugLog(@"Need to create double size of image %d",[ImageUtility isResizeDoubleSize:pickerImage withFrame:imageContainer.frame]);
        if([ImageUtility isResizeDoubleSize:pickerImage withFrame:imageContainer.frame])
        {
            CGSize doubleSize = CGSizeMake(imageContainer.frame.size.width*2, imageContainer.frame.size.height*2);
            pickerImage = [pickerImage imageByScalingToSize:doubleSize contentMode:UIViewContentModeScaleAspectFit];
        }
        
        imageContainer.contentMode = UIViewContentModeScaleAspectFit;
    }
    else
    {
        imageContainer.contentMode = UIViewContentModeCenter;
    }
    
    UIImageView *photoView = [[UIImageView alloc] initWithFrame:kPhotoContainer];
    photoView.image = pickerImage;
    photoView.userInteractionEnabled = YES;
    
    
   // UIImage *image = [self maskImage:shapImage withMask:photoView.image];
   // photoView.image = image;
    
    [self addGestureControl:photoView];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [GetAppDelegate() hideLoadingView];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark- AlertView delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(ENUM_TAG_PICTURE_SET == alertView.tag)
    {
        if(buttonIndex == 1)
        {
            [self takePhoto:nil];
        }
        else if(buttonIndex == 2)
        {
            [self selectPhoto:nil];
        }
        else
        {
            
        }
    }
    if(ENUM_TAG_REMOVE_PICTURE == alertView.tag)
    {
        if(buttonIndex == 1)
        {
            pickerImage = nil;
            // Remove the selected image picture.
            for (UIView *pickerView in imageContainer.subviews)
            {
                if (pickerView.tag == 99)
                {
                    [pickerView removeFromSuperview];
                }
            }
        }
    }
}

#pragma mark- Gesture delegate and call

- (void)addGestureControl:(UIImageView *) gestureImage
{
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureDetected:)];
    [panGestureRecognizer setDelegate:self];
    [gestureImage addGestureRecognizer:panGestureRecognizer];
    // create and configure the rotation gesture
    
    UIRotationGestureRecognizer *rotationGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotation:)];
    [rotationGestureRecognizer setDelegate:self];
    [gestureImage addGestureRecognizer:rotationGestureRecognizer];
    
    
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureDetected:)];
    [pinchGestureRecognizer setDelegate:self];
    [gestureImage addGestureRecognizer:pinchGestureRecognizer];
    gestureImage.tag = 99;
    [imageContainer addSubview:gestureImage];
    [self setClipToBound:shapImage];
}

- (void)handlePinch:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    //handle pinch...
}

-(void)handlePanGesture:(id)sender
{
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        firstX = [[sender view] center].x;
        firstY = [[sender view] center].y;
    }
    translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
    [[sender view] setCenter:translatedPoint];
}


- (void)panGestureDetected:(UIPanGestureRecognizer *)recognizer
{
        UIGestureRecognizerState state = [recognizer state];
        if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
        {
            CGPoint translation = [recognizer translationInView:recognizer.view];
            [recognizer.view setTransform:CGAffineTransformTranslate(recognizer.view.transform, translation.x, translation.y)];
            [recognizer setTranslation:CGPointZero inView:recognizer.view];
            
        }
}
-(void) rotation:(UIRotationGestureRecognizer *) sender
{
    if ([sender state] == UIGestureRecognizerStateBegan || [sender state] == UIGestureRecognizerStateChanged) {
        [sender view].transform = CGAffineTransformRotate([[sender view] transform], [(UIRotationGestureRecognizer *)sender rotation]);
        [(UIRotationGestureRecognizer *)sender setRotation:0];}
}

-(void) pinchGestureDetected:(UIPinchGestureRecognizer *) sender
{
    if ([sender state] == UIGestureRecognizerStateBegan || [sender state] == UIGestureRecognizerStateChanged)
    {
        [sender view].transform = CGAffineTransformScale([[sender view] transform], [sender scale], [sender scale]);
        [sender setScale:1];    }
}


-(void) longPressed:(UIGestureRecognizer *) sender
{
    DebugLog(@"LongPressed");
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


@end
