//
//  AppDelegate.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "Utility.h"
#import "NavigationController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
     NavigationController *nav = [NavigationController sharedNavigationController];
 
    [_window setRootViewController:nav];
    
    nav.navigationBar.hidden = YES;
    [_window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    if (self.alertView.visible)
    {
        [self.alertView dismissWithClickedButtonIndex:0 animated:NO];
    }
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {    
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
     [GetAppDelegate() hideFadeView];    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)showLoadingViewWithMsg:(NSString *)_message
{
    if(([Utility isDeviceIPad] && [Utility isIOS8]) || ![Utility isDeviceIPad])
    {
        [Utility resizeViewForiPhoneAndiPod:loadingView];
        messageView.layer.cornerRadius = 6;
        messageView.center = loadingView.center;
    }
    message.text = _message;
    [self.window addSubview:loadingView];
}

- (void)hideLoadingView
{
    [loadingView removeFromSuperview];
}

- (void)showFadeView
{
    if(([Utility isDeviceIPad] && [Utility isIOS8]) || ![Utility isDeviceIPad])
    {
        [Utility resizeViewForiPhoneAndiPod:fadeView];
    }
    [self.window addSubview:fadeView];
}

- (void)hideFadeView
{
    [fadeView removeFromSuperview];
}

- (void)showHelpMessage :(NSString *)_helpMessage  height:(CGFloat) height
{
    helpMessage.text = _helpMessage;
    CGSize maximumLabelSize = CGSizeMake([Utility deviceWidth]-20,1000);
    
    CGRect fontSize = [helpMessage.text boundingRectWithSize:maximumLabelSize
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14]}
                                         context:nil];   
    
    [helpView setFrame:CGRectMake(5, 40, [Utility deviceWidth]-10, fontSize.size.height+15)];
    //helpMessage.frame = fontSize;
    [helpMessage setFrame:CGRectMake(helpMessage.frame.origin.x, helpMessage.frame.origin.y, helpView.frame.size.width-30, fontSize.size.height)];
    
    helpView.layer.cornerRadius = 6;
    [self.window addSubview:helpView];
}
- (IBAction)hideHelpMessage
{
    [helpView removeFromSuperview];
}

@end
