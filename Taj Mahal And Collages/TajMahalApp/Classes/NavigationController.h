//
//  NavigationController.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 22/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MainViewController;


@interface NavigationController : UINavigationController


// static methods for NavigationController shared instance;
+ (NavigationController*)sharedNavigationController;

- (void)releaseNavigationController;
- (NSUInteger)supportedInterfaceOrientations;
@end
