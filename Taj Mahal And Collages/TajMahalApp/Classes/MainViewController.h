//
//  MainViewController.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UIPageViewControllerDataSource , UIPageViewControllerDelegate>
{
    
}
@property (nonatomic, assign) NSUInteger currentIndex;
@property (strong, nonatomic) UIPageViewController *pageController;
@end
