//
//  ChildViewController.h
//  TajMahalApp
//
//  Created by TIKAM CHANDRAKAR on 06/07/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildViewController : UIViewController
{
    
}
@property (nonatomic ,assign) NSInteger index;
@property (nonatomic ,strong) NSString *backgroundImage;
@end
