//
//  AppDelegate.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate>

{
    IBOutlet UIView *loadingView;
    IBOutlet UIView *fadeView;
    IBOutlet UIView *messageView;
    IBOutlet UILabel *message;
    
    IBOutlet UIView *helpView;
    IBOutlet UILabel *helpMessage;
}


@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) UIViewController *mainViewController;
@property (nonatomic,strong) UIAlertView *alertView;
@property (nonatomic,assign) NSInteger frameStatus;


- (void)showLoadingViewWithMsg:(NSString *)_message;
- (void)hideLoadingView;
- (void)showFadeView;
- (void)hideFadeView;
- (void)showHelpMessage :(NSString *)_helpMessage height:(CGFloat) height;
- (IBAction)hideHelpMessage;
@end

