//
//  ImageUtility.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 08/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Utility.h"

@interface ImageUtility : NSObject


/*
 @  All below 3 method are related to path creation and for saving the image 
 */

+ (NSString*)pathForDocumentResource: (NSString*)resourcePath;
+ (NSString*)pathForMainBundleResource: (NSString*)resourcePath;
+ (void)createChacheDirectory:(NSString *)kCacheFolderName;
/*
 @  saved image.
 */

+ (NSString *)saveImage: (UIImage*)image savedPath:(NSString *)path  imageInfo:(NSString *) imageInfo;

/*
 @ Get list of saved image.
 */
+ (NSMutableArray *)getListOfSavedImage:(BOOL)animated path:(NSString *)imagePath;

/*
 @ Creating image name with date
 */

+ (NSString *) imageNameFromDate;

/*
 @ Remove Cache image from document to use key name and path
 */

+ (void)removeCachedImageForKey: (NSString*) key path:(NSString *)path;

/*
 @ Get image from image name of key
 */
+ (NSDictionary  *)getImageFromKey:(NSString *)key imageData:(UIImage *)image;

/*
 @ removing cache file name  from document
 */
+ (void)removeImage:(NSString *)fileName;

/*
 @Obj : We can use it future if required to compress the image.
 */

+ (UIImage*)compressImage:(UIImage*)image;

/*
 @Obj : Get the rect witch contain the crop image.
 
 @param : Crop Image and conatiner frame.
 */

+ (UIImage *) getSubImageFrom: (UIImage *) img WithRect: (CGRect) rect;
/*
 @Obj : Check picker image size to conainter size if picker image is greater then 2 times of container image then we are just reduce the orignal size to just double of conainer size.
 
 @param : Actual Image and Container Frame.
 */
+ (BOOL) isResizeDoubleSize: (UIImage *)img withFrame: (CGRect) rect;

@end
