//
//  CameraGalleryController.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 09/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "CameraGalleryController.h"
#import "ConstantString.h"
#import "Utility.h"

@implementation CameraGalleryController

- (void)takePhotoFromCamera:(id)delegate
{
    _delegate = delegate;
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        if([_delegate respondsToSelector:@selector(mediaError: errorCode:)])
        {
            [_delegate mediaError:kNoCameraMessage errorCode:0];
        }
        return;
    }
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    // authorized
    if (authStatus == AVAuthorizationStatusAuthorized)
    {
        [self performSelector:@selector(openCamera) withObject:self afterDelay:1];
        return;
        
    }
    // denied or restricted
    else if (authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted)
    {
        if([_delegate respondsToSelector:@selector(mediaError:errorCode:)])
        {
            [_delegate mediaError:kSettingsMessageCamera errorCode:authStatus];
        }
        return;
    }
    // not determined, we should ask peremisison
    else if (authStatus == AVAuthorizationStatusNotDetermined)
    {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if (granted)
             {
                 // show not be invoked from main thread
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     [self performSelector:@selector(openCamera) withObject:self afterDelay:1];
                     
                 });
             }
             else
             {
                 // show not be invoked from main thread
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if([_delegate respondsToSelector:@selector(mediaError:errorCode:)])
                     {
                         [_delegate mediaError:kSettingsMessageCamera errorCode:authStatus];
                     }
                 });
             }
         }];
        
    }
    else
    {
        if([_delegate respondsToSelector:@selector(mediaError:errorCode:)])
        {
            [_delegate mediaError:kSettingsMessageCamera errorCode:authStatus];
        }
    }
    
}

- (void)takePhotoFromGallery:(id)delegate
{
    _delegate = delegate;
    ALAuthorizationStatus autorizationStatus = [ALAssetsLibrary authorizationStatus];
    // autorized
    if (autorizationStatus == ALAuthorizationStatusAuthorized)
    {
        [self performSelector:@selector(openGallery) withObject:self afterDelay:1];
        
    }
    // denied or restricted
    else if (autorizationStatus == ALAuthorizationStatusDenied || autorizationStatus == ALAuthorizationStatusRestricted)
    {
        if([_delegate respondsToSelector:@selector(mediaError:errorCode:)])
        {
            [_delegate mediaError:kSettingsMessageGallery errorCode:0];
        }
    }
    // not determined, we should ask peremisison
    else if (autorizationStatus == ALAuthorizationStatusNotDetermined)
    {
        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop)
         {
             if (group)
             {
                 //[self performSelector:@selector(openGallery) withObject:self afterDelay:1];
                 [self performSelector:@selector(openGallery) withObject:self afterDelay:1];
             }
             
         } failureBlock:^(NSError *error)
         {
             // show not be invoked from main thread
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 if([_delegate respondsToSelector:@selector(mediaError:errorCode:)])
                 {
                     [_delegate mediaError:kSettingsMessageGallery errorCode:0];
                 }
             });
         }];
    }
}


- (void) openCamera
{
    if([_delegate respondsToSelector:@selector(cameraPermissionGranted)])
    {
        [GetAppDelegate() hideFadeView];
        [_delegate cameraPermissionGranted];
    }
}

- (void) openGallery
{
    if([_delegate respondsToSelector:@selector(galleryPermissionGranted)])
    {
        [GetAppDelegate() hideFadeView];
        [_delegate galleryPermissionGranted];
    }
}

@end
