//
//  ConstantString.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#ifndef DexsatiFramwork_ConstantString_h
#define DexsatiFramwork_ConstantString_h


enum ALERT_VIEW_TAG
{
    ENUM_TAG_PICTURE_SET = 1,
    ENUM_TAG_REMOVE_PICTURE = 2
};




enum ADVANCE_EDIT_MODE
{
    ENUM_OPEN_FOROM_CUT_SCREEN = 1,
    ENUM_OPEN_SELECT_SCREEN = 2,    
};

enum
{
    ENUM_NOT_DRAW = 0,
    ENUM_IMAGE_DRAW = 1,
    ENUM_MOVING =2 ,
    ENUM_CLIPPED = 3,
    ENUM_FINISHED = 4,
    ENUM_CLEAR_IMAGE = 5,
    ENUMT_CROPPING = 6
    
};

#define kSettingsMessageGallery @"Please go to Privacy and change the settings to use Gallery"
#define kSettingsMessageCamera @"Please go to Privacy and change the settings to use Camera"
#define kNoCameraMessage   @"Device has no camera"
#define kSelectImageMsg    @"Please select at least one image"



#define kDeleteCreation @"Are you sure you want to delete Your Creation?. You will lose it permanently."
#define kDeleteCreationTitle @"Delete File ?"
#define kAdvanceCreation @"Are you sure you want to delete the file?. You will lose it permanently."



#define kOK             @"OK"
#define kWarning        @"Warning"
#define kError          @"Error"
#define kSorry          @"Sorry"
#define kClose          @"Close"
#define kYes            @"Yes"
#define kNo             @"No"
#define kCancel         @"Cancel"
#define kPleaseWait     @"Please wait..."
#define kCreationPath   @"Creation"
#define kCroping        @"Croping"

#define kImageNameSaved     @"MMM dd HH:mm:ss a"
#define kImageNameDisplay   @"MMM dd hh:mm a"

//Image Info
#define kImageName @"imageName"
#define kImageKey  @"imageKey"
#define kImageData @"imageData"

//Sharing content

#define kShareContent @"Say something about this photo"



//Default line width for cut screen
#define kCutDefaultLineWidth 3
#define kCutDefaultColor [UIColor blackColor]
#define kCutLineColorKey @"lineColor"
#define kEditAdvanceDefaultLineWidth 3



enum ENUM_FARME_COLLAGES
{
    ENUM_FRAME_SELECTED = 0,
    ENUM_COLLAGE_SELECTED = 1
};

#define kStickerName        @"StickerList"
#define kShapeName          @"ShapeList"


#endif
