//
//  CameraGalleryController.h
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 09/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
@protocol CameraGalleryDelegate <NSObject>

- (void) mediaError:(NSString *) errorMessage errorCode:(NSInteger) code;
- (void) cameraPermissionGranted;
- (void) galleryPermissionGranted;

@end

@interface CameraGalleryController : NSObject<UIAlertViewDelegate>
{
    
}

@property (weak, nonatomic) id<CameraGalleryDelegate> delegate;

- (void)takePhotoFromCamera:(id)delegate;
- (void)takePhotoFromGallery:(id)delegate;
@end
