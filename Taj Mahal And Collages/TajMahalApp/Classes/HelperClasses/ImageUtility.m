//
//  ImageUtility.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 08/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "ImageUtility.h"
#import "ConstantString.h"

@implementation ImageUtility
+ (NSString*)pathForDocumentResource: (NSString*)resourcePath
{
    if(!resourcePath || ![resourcePath length])
        return nil;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[paths objectAtIndex: 0] stringByAppendingPathComponent: resourcePath];
}

//--------------------------------------------------------------------
// path for main bundle resources
//--------------------------------------------------------------------

+ (NSString*)pathForMainBundleResource: (NSString*)resourcePath
{
    if(!resourcePath || ![resourcePath length])
        return nil;
    
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: resourcePath];
}

+ (void)createChacheDirectory:(NSString *)kCacheFolderName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *cachePath = [self pathForDocumentResource: kCacheFolderName];
    
    DebugLog(@"Path %d",[fileManager fileExistsAtPath: cachePath]);
    if (![fileManager fileExistsAtPath: cachePath])
    {
        [fileManager createDirectoryAtPath: cachePath
               withIntermediateDirectories: NO
                                attributes: nil
                                     error: nil];
    }
}
+ (NSString *)saveImage: (UIImage*)image savedPath:(NSString *)path  imageInfo:(NSString *)editImageName
{
    if (image != nil)
    {
        [self createChacheDirectory:path];      
        
        NSString *imageName = editImageName;
        if(editImageName == nil )
        {
            imageName = [self imageNameFromDate];
        }
        
        if([editImageName isEqualToString:@"cropped"] )
        {
            imageName = [NSString stringWithFormat:@"cropped%@", [self imageNameFromDate]];
        }

        
        NSString *dirPath = [self pathForDocumentResource: path];
        dirPath = [dirPath stringByAppendingPathComponent: imageName];
        
      //UIImage  *compressImage = [self compressImage:image];
        NSData* data = UIImagePNGRepresentation(image);
       [data writeToFile:dirPath atomically:YES];
        return imageName;
    }
    return nil;    
}

+ (NSMutableArray *)getListOfSavedImage:(BOOL)animated path:(NSString *)imagePath
{
    NSMutableArray  *arrayOfImages = [[NSMutableArray alloc]init];
    NSError *error = nil;
    
    NSString *stringPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    stringPath = [NSString stringWithFormat:@"%@/%@",stringPath,imagePath];
    NSArray *filePathsArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: stringPath  error:&error];
    //NSLog( @"File Path %@",filePathsArray);
    //NSLog( @"String  Path %@",stringPath);
   // NSLog( @"File Path %@",filePathsArray);
    for(int i=0;i<[filePathsArray count];i++)
    {
        NSString *strFilePath = [filePathsArray objectAtIndex:i];
        if ([[strFilePath pathExtension] isEqualToString:@"jpg"] || [[strFilePath pathExtension] isEqualToString:@"JPG"] || [[strFilePath pathExtension] isEqualToString:@"png"] || [[strFilePath pathExtension] isEqualToString:@"PNG"])
        {
            NSString *imagePath = [[stringPath stringByAppendingString:@"/"] stringByAppendingString:strFilePath];
            NSString *nameWithoutExtention = [strFilePath stringByDeletingPathExtension];
            
            //nameWithoutExtention = [Utility dateFromatConverter:nameWithoutExtention currentFromat:kImageNameSaved newFormat:kImageNameDisplay];
            
            nameWithoutExtention = [self displayImageName:nameWithoutExtention];
            
            NSLog(@"Path Of Image %@",nameWithoutExtention );
            NSData *data = [NSData dataWithContentsOfFile:imagePath];
            if(data)
            {
                UIImage *image = [UIImage imageWithData:data];
                NSDictionary *imageInfo = [NSDictionary dictionaryWithObjectsAndKeys:nameWithoutExtention,kImageName,image,kImageData, strFilePath, kImageKey, nil];
                
                [arrayOfImages addObject:imageInfo];
            }
        }
    }
    arrayOfImages  = (NSMutableArray *)[[arrayOfImages reverseObjectEnumerator] allObjects];
    return arrayOfImages;
}


+ (NSString *) imageNameFromDate
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kImageNameSaved];
    [format setLocale:[NSLocale currentLocale]];
    [format setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    
    NSLog(@"Date %@",dateString);
    return  [NSString stringWithFormat:@"%@.png",dateString];
}


+ (NSString *)displayImageName :(NSString *)imageDate
{
        DebugLog(@"date %@", imageDate);
        NSString *dateString = imageDate;
        NSString *resultSctring;
        NSRange lastComma = [dateString rangeOfString:@":" options:NSBackwardsSearch];
        if(lastComma.location != NSNotFound) {
            NSRange replaceRange = NSMakeRange(lastComma.location, 3);
            {
                resultSctring = [dateString stringByReplacingCharactersInRange:replaceRange withString:@""];
                DebugLog(@"result %@", resultSctring);
            }
        }
        return resultSctring;
}

+ (void)removeCachedImageForKey: (NSString*) key path:(NSString *)path
{
    if (key != nil)
    {
        NSError *error = nil;
        NSString *dirPath = [self pathForDocumentResource: path];
        dirPath = [dirPath stringByAppendingPathComponent: key];
        NSFileManager *manager = [NSFileManager defaultManager];
        
        [manager removeItemAtPath: dirPath error: &error];
    }
}

+ (NSDictionary  *)getImageFromKey:(NSString *)key imageData:(UIImage *)image
{
    DebugLog(@"Image Detail %@",key);
    NSString *nameWithoutExtention = [key stringByDeletingPathExtension];
   // NSString *imageName = [Utility dateFromatConverter:nameWithoutExtention currentFromat:kImageNameSaved newFormat:kImageNameDisplay];
    NSString *imageName = [self displayImageName:nameWithoutExtention];
    
        NSDictionary *imageInfo = [NSDictionary dictionaryWithObjectsAndKeys:imageName,kImageName,image,kImageData, key, kImageKey, nil];
    
    DebugLog(@" image Name %@",imageInfo);
        return imageInfo;
}

+ (void)removeImage:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        NSLog(@"Removed Success");
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

// compress Image (Praveen added on Tue 13 Jan 2015)
+ (UIImage *)compressImage:(UIImage *)image
{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    //UIGraphicsBeginImageContext(rect.size)
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}


+ (UIImage *) getSubImageFrom: (UIImage *) img WithRect: (CGRect) rect {
    
    //UIGraphicsBeginImageContextWithOptions(rect.size ,YES, -1);
    
       NSLog(@"rect width: %f, height %f",rect.size.width, rect.size.height);
    UIGraphicsBeginImageContext(rect.size);
   
    CGContextRef context = UIGraphicsGetCurrentContext();
   //  CGContextClearRect(context, rect);
    // translated rectangle for drawing sub image
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y , img.size.width, img.size.height);
    // clip to the bounds of the image context
    // not strictly necessary as it will get clipped anyway?
    [[UIColor clearColor] setFill];
    
    //  Fill all context with background image
    CGContextFillRect(context, rect);
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    // draw image
    [img drawInRect:drawRect];
    
    // grab image
    UIImage* subImage = UIGraphicsGetImageFromCurrentImageContext();
    NSLog(@"SubImage width: %f, height %f",subImage.size.width, subImage.size.height);
    
    UIGraphicsEndImageContext();
    
    return subImage;
}
+ (BOOL) isResizeDoubleSize: (UIImage *)img withFrame: (CGRect) rect
{
    if(img.size.width > rect.size.width * 2 || img.size.height > rect.size.height * 2)
    {
        return YES;
    }
    return NO;
    
}


@end
