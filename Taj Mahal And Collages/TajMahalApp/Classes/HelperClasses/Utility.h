//
//  Utility.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "UIImage+SimpleResize.h"
#import "ImageUtility.h"
#import "ConstantString.h"
#import "NavigationController.h"

#ifdef DEBUG
#define DebugLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define DebugLog(format, ...)
#endif

AppDelegate* GetAppDelegate();

@interface Utility : NSObject
{
    
}
+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex;

+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex
                      tagValue:(int)tag;
+ (NSString*)appName;

+ (CGFloat)deviceWidth;
+ (CGFloat)deviceHeight;
+ (CGFloat)getXpoint:(CGFloat)width frameWidth:(CGFloat)frameWidth;
+ (CGFloat)getYpoint:(CGFloat)width frameHeight:(CGFloat)frameHeight;
+ (CGFloat)getYpointForViewController:(CGFloat)height frameHeight:(CGFloat)frameHeight;
+ (void)resizeViewForiPhoneAndiPod:(UIView*)targetView;
+ (BOOL) isDeviceIPad;
+ (UIColor *)getCurrentColor;
+ (UIColor *)setCurrentColor:(UIColor *)currentColor;
+ (BOOL)isIOS8;
+ (BOOL)isPhone6;
+ (BOOL)isPhone6Plus;
+ (NSInteger)getPadding;
+ (BOOL)isPhone5;


@end
