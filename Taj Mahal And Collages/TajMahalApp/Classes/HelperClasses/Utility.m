//
//  Utility.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "Utility.h"
#import "AppDelegate.h"



AppDelegate* GetAppDelegate()
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}
@implementation Utility

+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex
{
    
    
    if (GetAppDelegate().alertView != nil)
    {
        GetAppDelegate().alertView.delegate = nil;
        GetAppDelegate().alertView = nil;
    }
    
    GetAppDelegate().alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:delegate
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:nil];
    for (NSString *title in buttonTitles)
    {
        [GetAppDelegate().alertView addButtonWithTitle: title];
    }
    GetAppDelegate().alertView.cancelButtonIndex = cancelButtonIndex;
    [GetAppDelegate().alertView show];
}

+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex
                      tagValue:(int)tag
{
    
    if (GetAppDelegate().alertView != nil)
    {
        GetAppDelegate().alertView.delegate = nil;
        GetAppDelegate().alertView = nil;
    }
    
    GetAppDelegate().alertView = nil;
    GetAppDelegate().alertView	= [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:delegate
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:nil];
    
    [GetAppDelegate().alertView setTag:tag];
    
    for (NSString *title in buttonTitles)
    {
        [GetAppDelegate().alertView addButtonWithTitle: title];
    }
    
    GetAppDelegate().alertView.cancelButtonIndex = cancelButtonIndex;
    
    [GetAppDelegate().alertView show];
}
+ (NSString*)appName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}



+ (CGFloat)deviceWidth
{
     DebugLog(@"width %f", [UIScreen mainScreen].bounds.size.width);
    return [UIScreen mainScreen].bounds.size.width;
}

+ (CGFloat)deviceHeight
{
    DebugLog(@"Heifht %f", [UIScreen mainScreen].bounds.size.height);
    return [UIScreen mainScreen].bounds.size.height;
}
+ (CGFloat)getXpoint:(CGFloat)width frameWidth:(CGFloat)frameWidth
{
//    DebugLog(@"Top frameWidth %f",frameWidth);
//    DebugLog(@"width %f",width);
//    DebugLog(@"return; %f", (frameWidth - width)/2);
    
    return (frameWidth - width)/2;
}

+ (CGFloat)getYpoint:(CGFloat)height frameHeight:(CGFloat)frameHeight
{
    return -(frameHeight - height)/2;
}


+ (CGFloat)getYpointForViewController:(CGFloat)height frameHeight:(CGFloat)frameHeight
{
    //DebugLog(@"Top frameHeight %f",frameHeight);
   //  DebugLog(@"Height %f",height);
    return (frameHeight - height)/2;
}



+ (void)resizeViewForiPhoneAndiPod:(UIView*)targetView
{
    // if device is iPad then we dont need to set frame size for target view
    //if ([Helper isDeviceIPad])  return;
    if([Utility isDeviceIPad] && [[[UIDevice currentDevice] systemVersion] doubleValue] < 8.0)
    {
        return;
    }
    // this frame setting is for iPhone 5 and other iPhone/iPodTouch devices
    targetView.frame =  CGRectMake(targetView.frame.origin.x,
                                   targetView.frame.origin.y,
                                   [[UIScreen mainScreen]bounds].size.width,
                                   [[UIScreen mainScreen]bounds].size.height);
}

+ (BOOL) isDeviceIPad
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
#endif
    return NO;
}

+ (UIColor *)getCurrentColor
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:kCutLineColorKey])
    {
        NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:kCutLineColorKey];
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        return color;
    }
    return kCutDefaultColor;
}

+ (UIColor *)setCurrentColor:(UIColor *)currentColor
{
     NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:currentColor];
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:kCutLineColorKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return currentColor;
}

+ (BOOL)isIOS8
{
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 8.0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+ (BOOL)isPhone6
{
    if([self deviceWidth] == 375)
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isPhone5
{
    if([self deviceHeight] == 568)
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isPhone6Plus
{
    if([self deviceWidth] > 375)
    {
        return YES;
    }
    
    return NO;
}

+(NSInteger) getPadding
{
    
    if ([self isPhone6Plus]) {
        return 80;
    }
    else if([self isPhone6])
    {
         return 76;
    }
    else
    {
        return 73;
    }
}


@end
