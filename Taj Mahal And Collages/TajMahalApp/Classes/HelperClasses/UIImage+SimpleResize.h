//  UIImage+SimpleResize.h
//
//  Created by Robert Ryan on 5/19/11.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/** Image resizing category.
 *
 *  Modified by Robert Ryan on 5/19/11.
 *
 *  Inspired by http://ofcodeandmen.poltras.com/2008/10/30/undocumented-uiimage-resizing/
 *  but adjusted to support AspectFill and AspectFit modes.
 */

@interface UIImage (SimpleResize)

/** Resize the image to be the required size, stretching it as needed.
 *
 * @param newSize      The new size of the image.
 * @param contentMode  The `UIViewContentMode` to be applied when resizing image.
 *                     Either `UIViewContentModeScaleToFill`, `UIViewContentModeScaleAspectFill`, or
 *                     `UIViewContentModeScaleAspectFit`.
 *
 * @return             Return `UIImage` of resized image.
 */

- (UIImage*)imageByScalingToSize:(CGSize)newSize contentMode:(UIViewContentMode)contentMode;

/** Crop the image to be the required size.
 *
 * @param bounds       The bounds to which the new image should be cropped.
 *
 * @return             Cropped `UIImage`.
 */

- (UIImage *)imageByCroppingToBounds:(CGRect)bounds;

/** Resize the image to be the required size, stretching it as needed.
 *
 * @param newSize The new size of the image.
 *
 * @return        Resized `UIImage` of resized image.
 */

- (UIImage*)imageByScalingToFillSize:(CGSize)newSize;

/** Resize the image to fill the rectange of the specified size, preserving the aspect ratio, trimming if needed.
 *
 * @param newSize The new size of the image.
 *
 * @return        Return `UIImage` of resized image.
 */

- (UIImage*)imageByScalingAspectFillSize:(CGSize)newSize;

/** Resize the image to fit within the required size, preserving the aspect ratio, with no trimming taking place.
 *
 * @param newSize The new size of the image.
 *
 * @return        Return `UIImage` of resized image.
 */

- (UIImage*)imageByScalingAspectFitSize:(CGSize)newSize;


// Created By Xymob

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;

/** Check the image size is greater then container or not if greater then then we set mode aspect fit other wise center mode..
 *
 * @param newSize The new size of the image.
 *
 * @return   content mode.
 */
 - (UIViewContentMode )getContentMode :(CGSize)newSize;


/** Get the ratio of resize image
 *
 * @param newSize The new size of the image.
 *
 * @return  ratio.
 */
- (CGFloat )getImageRatio :(CGSize)newSize;


/** Some of the device (iPad) , when we pick image from camera and shwoing in context or view it shown up  90 degree rotated format .  this method will correct reverce it,
 *
 * @param newSize The new size of the image.
 *
 * @return  set right format of image
 */

-(UIImage*) rotate:(UIImage*) src andOrientation:(UIImageOrientation)orientation;

@end
