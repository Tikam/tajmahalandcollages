//
//  NavigationController.m
//  CutCopyPaste
//
//  Created by TIKAM CHANDRAKAR on 22/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "NavigationController.h"
#import "MainViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "CreateFrameOrCollageViewController.h"

static NavigationController *navigationControllerInstance = nil;
@interface NavigationController ()

@end

@implementation NavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)releaseNavigationController
{
    navigationControllerInstance = nil;
}

+ (NavigationController *)sharedNavigationController
{
    if (!navigationControllerInstance)
    {
        MainViewController *root = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
        navigationControllerInstance = [[NavigationController alloc] initWithRootViewController:root];
    }
    return navigationControllerInstance;
}

- (void) pushViewController: (UIViewController*) viewController animated: (BOOL) animated
{
    [GetAppDelegate() hideHelpMessage];
    
    [super pushViewController: viewController animated: animated];
}
- (UIViewController *) popViewControllerAnimated: (BOOL) animated
{
    [GetAppDelegate() hideHelpMessage];
    return [super popViewControllerAnimated: animated];
}

- (void)popUpToRootViewController:(NSInteger)index
{
    [super popToRootViewControllerAnimated:YES];
}

- (UIViewController*) getTopViewControllerforTabController
{
    UIViewController *topController = [NavigationController sharedNavigationController].topViewController;
  
    return topController;
}

////
- (BOOL)shouldAutorotate
{
    BOOL autoRotate = YES;
    if ([self getTopViewControllerforTabController ] != nil)
    {
        autoRotate = [[self getTopViewControllerforTabController] shouldAutorotate];
    }
    return autoRotate;
}

- (NSUInteger)supportedInterfaceOrientations
{
    NSUInteger supportedOrientation = UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft;
    if ([[self getTopViewControllerforTabController] isKindOfClass:[CreateFrameOrCollageViewController class]] )
    {
//        if(supportedOrientation == UIInterfaceOrientationLandscapeLeft)
//        {
//            return supportedOrientation;
//        }
         NSUInteger supportedOrientation = UIInterfaceOrientationLandscapeLeft;
        supportedOrientation = [[self getTopViewControllerforTabController] supportedInterfaceOrientations];
    }
    
    DebugLog(@"Support Orinetation %lu",(unsigned long)supportedOrientation);
    return supportedOrientation;
}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    if ([[self getTopViewControllerforTabController] isKindOfClass:[CreateFrameOrCollageViewController class]] )
//    {
//        return [[self getTopViewControllerforTabController ] preferredInterfaceOrientationForPresentation];
//    }
//    else
//    {
//        return UIInterfaceOrientationPortrait;
//    }
//}


@end
