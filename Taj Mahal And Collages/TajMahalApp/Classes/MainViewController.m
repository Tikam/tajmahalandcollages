//
//  MainViewController.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "MainViewController.h"
#import "Utility.h"
#import "ChildViewController.h"
#import "CreateFrameOrCollageViewController.h"
@interface MainViewController ()
{
    IBOutlet UIImageView *backgroundImageView;
    NSArray *imageArray ;
}

@end

@implementation MainViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
        UIImage *backgroundImage = [[UIImage alloc] init];
        if ([Utility isPhone6Plus]) {
            backgroundImage = [UIImage imageNamed:@"414x736@3xg"];
        }
        else if([Utility isPhone6]){
            backgroundImage = [UIImage imageNamed:@"375x-667@2x"];
        }
        else if([Utility isPhone5])
        {
            backgroundImage = [UIImage imageNamed:@"320x568@2x"];
 
        }
        else{
             backgroundImage = [UIImage imageNamed:@"320x480@2x"];
        }
        
        [backgroundImageView setImage:backgroundImage];
        
        
        imageArray = [NSArray arrayWithObjects:@"page1.jpg",@"page2.jpg",@"page3.jpg", nil];
        
    }
    return self;
}

#pragma mark View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // initial setUp
    [Utility resizeViewForiPhoneAndiPod:self.view];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:CGRectMake(30, 60, [Utility deviceWidth] - 60, 300)];
    
    [self setViewInScreen];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    [self setViewInScreen];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (ChildViewController *)viewControllerAtIndex:(NSUInteger)index
{
    _currentIndex = index;
    
    ChildViewController *childViewController = [[ChildViewController alloc] initWithNibName:@"ChildViewController" bundle:nil];
    childViewController.index = index;
    //childViewController.contentsArray = self.contentArray;
    childViewController.backgroundImage = [imageArray objectAtIndex:index];
    return childViewController;
}

- (void) setViewInScreen
{
    ChildViewController *childViewController = [self viewControllerAtIndex:_currentIndex];
    
    NSArray *viewControllers = [NSArray arrayWithObject:childViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(ChildViewController *)viewController index];
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(ChildViewController *)viewController index];
    index++;
    
    if (index == imageArray.count) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    // The number of items reflected in the page indicator.
    return imageArray.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    
    //UIPageControl *pageControl = [UIPageControl appearance];
  //  pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    //pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    
    return _currentIndex;
}


#pragma marks- IBActions

- (IBAction)leftSwipe
{
    
    if (_currentIndex == 0) {
        return ;
    }
    _currentIndex--;
    [self setViewInScreen];
}

- (IBAction)rightSwipe
{   
    if (_currentIndex == imageArray.count-1) {
       
        return ;
    }
     _currentIndex++;
    [self setViewInScreen];
}
- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        GetAppDelegate().frameStatus = ENUM_FRAME_SELECTED;
    }
    else
    {
        GetAppDelegate().frameStatus = ENUM_COLLAGE_SELECTED;
    }
}

- (IBAction)doneButtonClick
{      
    CreateFrameOrCollageViewController *createFrameOrCollageViewController = [[CreateFrameOrCollageViewController alloc] init];
    [createFrameOrCollageViewController setSelectedImageName:[imageArray objectAtIndex:_currentIndex]];
   
    [[NavigationController sharedNavigationController] pushViewController:createFrameOrCollageViewController animated:YES];
    
}

#pragma marks- Orientation Handlling.

-(BOOL)shouldAutorotate
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if(orientation != UIDeviceOrientationPortrait)
    {
        return NO;
    }
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    NSUInteger supportedOrientations = UIInterfaceOrientationMaskPortrait;
    return supportedOrientations;
}

@end
