//
//  ShapeCollectionViewCell.m
//  TajMahalApp
//
//  Created by TIKAM CHANDRAKAR on 07/07/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "ShapeCollectionViewCell.h"

@implementation ShapeCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    self.layer.borderColor = [[UIColor alloc] initWithRed:211.0/255.0 green:211.0/255.0 blue:211.0/255.0 alpha:1].CGColor;
    self.layer.borderWidth = 1.0;
    self.layer.cornerRadius = 5;
}

@end
