//
//  CreateFrameOrCollageViewController.h
//  TajMahalApp
//
//  Created by TIKAM CHANDRAKAR on 06/07/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResouceController.h"
#import "CameraGalleryController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface CreateFrameOrCollageViewController : UIViewController<UICollectionViewDataSource , UICollectionViewDelegate,CameraGalleryDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>
{
    
}
@property(nonatomic, strong) NSString *selectedImageName;

- (void)setBackgroundImage;

@end
