//
//  ShapeCollectionViewCell.h
//  TajMahalApp
//
//  Created by TIKAM CHANDRAKAR on 07/07/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShapeCollectionViewCell : UICollectionViewCell
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImage;
@end
